module.exports = {
  home: {
    name: 'Home',
    path: '/',
    component: ''
  },
  login: {
    name: 'Login',
    path: '/login',
    component: ''
  },
  recipe: {
    name: 'Recipe',
    path: '/recipe/:name',
    component: ''
  },
  recipeAdmin: {
    path: '/admin/recipe',
    name: 'RecipeAdmin',
    component: ''
  },
  ingredientAdmin: {
    path: '/admin/ingrediant',
    name: 'IngridiantAdmin',
    component: ''
  }
}
