const api = require('../services/api')
const table = 'recipe'

exports.list = async function (client = api) {
  return await client.list({ table: table })
}

exports.create = async function (data, client = api) {
  return await client.create({ table: table, object: data })
}

exports.update = async function (recipieId, data, client = api) {
  return await client.update({
    table: table,
    filter: { id: recipieId },
    object: data
  })
}

exports.delete = async function (recipieId, client = api) {
  return await client.delete({ table, filter: { id: recipieId } })
}
