const api = require('../services/api')

exports.login = async function (username, password, client = api) {
    return await client.login(username, password)
}

exports.logout = async function (client = api) {
    return await client.logout()
}

exports.isLoggedIn = async function (client = api) {
    return client.isLoggedIn()
}