import ingrediantController from './ingredient'
import recepieController from './recepie'
import userController from './user'

const recipes = 'recipes'
const ingredients = 'ingredients'
const user = 'user'

export default {
  methods: {
    userLogin: function (username, password) {
      userController.login(username, password)
        .then(result => {
          this.showSuccess('login successful')
          this.setItem(user, result)
        })
        .catch(err => this.showError(err))
    },
    userLogout: function () {
      userController.logout()
       .then(result => this.$root.logout())
       .catch(err => this.showError(err))
    },
    recipeCreate: function (recipe) {
      recepieController.create(recipe)
        .then(result => this.showSuccess(result))
        .catch(err => this.showError(err))
    },
    recipeList: function () {
      recepieController.list()
        .then(result => this.setItems(recipes, result))
        .catch(err => this.showError(err))
    },
    recipeUpdate: function (recipe) {
      recepieController.update(recipe.id, recipe)
        .then(result => this.showSuccess(result))
        .catch(err => this.showError(err))
    },
    recipeDelete: function (recipe) {
      recepieController.delete(recipe.id)
        .then(result => this.showSuccess(result))
        .catch(err => this.showError(err))
    },
    ingrediantCreate: function (ingredient) {
      console.log('calling create..')
      ingrediantController.create(ingredient)
        .then(result => this.showSuccess(result))
        .catch(err => this.showError(err))
    },
    ingrediantGetByRecipe: function (recipeId) {
      ingrediantController.getByRecipe(recipeId)
        .then(result => this.setItem(ingredients, result))
        .catch(err => this.showError(err))
    },
    ingrediantUpdate: function (ingredient) {
      ingrediantController.update(ingredient.id, ingredient)
        .then(result => this.showSuccess('ingredients updated'))
        .catch(err => this.showError(err))
    },
    ingrediantDeleteByRecipe: function (recipe) {
      ingrediantController.deleteByRecipe(recipe.id)
        .then(result => this.showSuccess(result))
        .catch(err => this.showError(err))
    },
    showSuccess: function (success) {
      console.log(success)
      this.$root.success = success

      setTimeout(() => {
        this.$root.success = ''
      }, 3000)
    },
    showError: function (error) {
      if (error instanceof Error) {
        this.$root.error = error.message
      } else {
        this.$root.error = error
      }

      setTimeout(() => {
        this.$root.error = ''
      }, 3000)
    },
    showValidationError: function (error) {
      this.$root.error = error[0]
    },
    setItem: function (keyname, data) {
      this.$root[keyname] = data
    },
    setItems: function (keyname, data) {
      this.$root[keyname] = []
      data.forEach((item) => this.$root[keyname].push(item))
    }
  }
}
