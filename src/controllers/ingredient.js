const api = require('../services/api')
const table = 'ingredient'

exports.getByRecipe = async function (recipeId, client = api) {
  return await client.get({
    table: table,
    filter: { recipe: recipeId }
  })
}

exports.create = async function (data, client = api) {
  return await client.create({ table: table, object: data })
}

exports.update = async function (ingrediantId, data, client = api) {
  return await client.update({
    table: table,
    filter: { id: ingrediantId },
    object: data
  })
}


exports.deleteByRecipe = async function (recipeId, client = api) {
  return await client.delete({
    table: table,
    filter: { recipe: recipeId }
  })
}
