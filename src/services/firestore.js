const uuid = require('uuid')
const firebase = require('firebase')
require('firebase/firestore')

const config = require('../config').firebase
const app = firebase.default.initializeApp(config)
const firestore = app.firestore()
const auth = app.auth()

exports.login = async function (username, password, authClient = auth) {
  const response = await authClient.signInWithEmailAndPassword(username, password)
  return response.user
}

exports.logout = async function (authClient = auth) {
  const response = await authClient.signOut()
  return response
}

exports.isLoggedIn = function (authClient = auth) {
  return (authClient.currentUser) ? true : false
}

exports.list = async function (table, store = firestore) {
  const snapShot = await store.collection(table).get()
  return snapShot.docs.map((doc) => doc.data())
}

exports.get = async function (table, filter, store = firestore) {
  const tableRef = store.collection(table)
  const filterArray = keyValueToArray(filter)
  // console.log(filterArray)
  const snapShot = await tableRef.where(
    filterArray[0], '==', filterArray[1]
  ).get()

  return snapShot.docs.map((doc) => doc.data())[0]
}

exports.create = async function (table, object, store = firestore) {
  const docId = uuid.v1()
  object.id = docId
  const tableRef = store.collection(table)
  return await tableRef.doc(docId).set(object)
}

exports.update = async function (table, filter, object, strore = firestore) {
  const tableRef = strore.collection(table)
  const response = tableRef.doc(filter.id).update(object)
  return response
}

exports.delete = async function (table, filter, store = firestore) {
  const tableRef = store.collection(table)
  const response = await tableRef.doc(filter.id).delete()
  return response
}

function keyValueToArray (filter) {
  const keys = Object.keys(filter)
  if (keys.length > 1) {
    throw Error(`too many filters provided ${filter}`)
  }

  return [keys[0], filter[keys[0]]]
}
