const dataApi = require('./firestore')
const authApi = require('./firestore')

exports.login = async function (username, password, auth = authApi) {
  return await auth.login(username, password)
}

exports.logout = async function (auth = authApi) {
  return await auth.logout()
}

exports.isLoggedIn = async function (auth = authApi) {
  return await auth.isLoggedIn()
}

exports.list = async function ({ table }, api = dataApi) {
  return await api.list(table)
}

exports.get = async function ({ table, filter }, api = dataApi) {
  return await api.get(table, filter)
}

exports.create = async function ({ table, object }, api = dataApi) {
  return await api.create(table, object)
}

exports.update = async function ({ table, filter, object }, api = dataApi) {
  return await api.update(table, filter, object)
}

exports.delete = async function ({ table, filter }, api = dataApi) {
  return await api.delete(table, filter)
}
