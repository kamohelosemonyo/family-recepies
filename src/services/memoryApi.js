const data = {
  recipe: [
    { name: 'pancake', id: '154843b0-a30a-11eb-938e-3d764bc5aa96', views: 10, datecreated: '2021-01-01' },
    { id: '5ff93f30-a30b-11eb-b747-5f7810e3e488', name: 'flat bread', views: 3, datecreated: '2021-01-01' },
    {
      id: '70629a10-a30b-11eb-87d0-454c69402e76',
      name: 'sweet pepper meat balls', views: 15, datecreated: '2021-01-01'
    },
    { name: 'brown muffins', id: 'a25a4e00-a315-11eb-93a1-f1fc7dfb4b38', views: 1, datecreated: '2021-04-01' },
    {
      id: 'e0bad340-a30b-11eb-b9a1-c5154d1ce9ac',
      name: 'chutney chicken', views: 6, datecreated: '2021-04-01'
    }
  ],
  ingredient: [
    {
      recipe: '154843b0-a30a-11eb-938e-3d764bc5aa96',
      items: [
        { name: 'milk', quantity: '1 cup' },
        { name: 'eggs', quantity: '2' },
        { name: 'oil', quantity: '1 teaspoon' },
        { name: 'sugar', quantity: '1 treaspoon' },
        { name: 'cynamon', quantity: '' }
      ],
      instructions: [
        'Use two sperate containers to mix liquid and none liquid ingrediants',
        'Poor in the milk',
        'Crack and mix the eggs into the milk',
        'Add the oil into the milk and mix well',
        'Add the flower into a bowl',
        'Add the sugar into the flower bowl and mix well',
        'Poor the milk mix into the bowl of flour and mix well',
        'Use a none stick frying pan add a little bit of oil',
        'Put on the stove at a medium heat wait for the pan to heat up',
        'Poor the mix into the pan',
        'Wait for the mix to have small bubbles ontop to indicate its ready to be turned',
        'Turn the pan cake and wait till the other side is complete',
        'remove the pancake add cynamon and sugar then roll the pancake',
        'Serve and enjoy your meal'
      ]
    }
  ]
}

exports.list = function (table) {
  if (data[table] !== undefined) {
    return data[table]
  } else {
    return []
  }
}

exports.get = function (table, filter) {
  if (table === 'ingredient') {
    for (const ingredient of data[table]) {
      const keys  = Object.keys(filter)

      if (ingredient[keys[0]] === filter[keys[0]]) {
        return ingredient
      }
    }

    return {}
  } else {
    return []
  }
}

exports.create = function (table, object) {
  object.id = new Date().toString()
  const items = getTable(table)
  items.push(object)
  console.log(items)
  setTable(table, items)
  return 'create successful'
}

exports.update = function (table, filter, object) {
  const items = getTable(table)
  const filteredItems = []

  filterItems(items, filter, (item) => {
    filteredItems.push(item)
  })

  filteredItems.forEach((item) => {
    const index = items.indexOf(item)
    items[index] = object 
  })

  setTable(table, items)
  return 'update successful'
}

exports.delete = function (table, filter) {
  const items = getTable(table)
  const filteredItems = []

  filterItems(items, filter, (item) => {
    filteredItems.push(item)
  })

  filteredItems.forEach((item) => {
    items.splice(items.indexOf(item), 1)
  })

  setTable(table, items)
  return 'delete successful'
}

function getTable (table) {
  const items = data[table]
  if (items === undefined) {
    return []
  } else {
    return items
  }
}

function setTable (table, items) {
  data[table] = items
}

function filterItems (items, filter, callback) {
  const filterKeys = Object.keys(filter)

  for (const key of filterKeys) {
    for (const item of items) {
      if (item[key] === filter[key]) {
        callback(item)
      }
    }
  }
}
