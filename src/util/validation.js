exports.isString = isString
exports.isNumeric = isNumeric
exports.isAlpha = isAlpha
exports.isAlphaNumeric = isAlphaNumeric

exports.validate = function (object, metadata) {
  validateObject(object)
  validateMetaData(metadata)
  containsMatchingKeys(object, metadata)
  const result = validateObjectData(object, metadata)
  return result
}

function validateObject (object) {
  isObject(object)
  isEmpty(object)
}

function validateMetaData (metadata) {
  isObject(metadata)
  isEmpty(metadata)
}

function containsMatchingKeys (actual, expected) {
  const actualKeys = Object.keys(actual)
  const expectedKeys = Object.keys(expected)

  for (const key of actualKeys) {
    if (expectedKeys.indexOf(key) < 0) {
      throw Error(`undefinded key in metadata found in object ${key}`)
    }
  }
}

function validateObjectData (object, metadata) {
  const objectKeys = Object.keys(object)
  const metaKeys = Object.keys(metadata)
  const results = []
  metaKeys.forEach((key) => isObject(metadata[key]))

  objectKeys.forEach(function (key) {
    const result = isValid(key, object[key], metadata[key])

    if (result !== true) {
      results.push(result)
    }
  })

  return results
}

function isObject (object) {
  if (typeof object !== 'object') {
    throw Error(`invalid object provided ${object}`)
  }
}

function isEmpty (object) {
  if (Object.keys(object).length <= 0) {
    throw Error(`empty object provided ${object}`)
  }
}

function isValid (keyname, objectKey, metakey) {
  let type = metakey.type
  if (type === undefined) {
    throw Error(`metadata for ${keyname} is invalid. type is undefined`)
  }

  if (typeof type !== 'string') {
    throw Error(`metadata for ${keyname} is invalid. type is not a string value`)
  }

  type = type.toLowerCase()
  switch (type) {
    case 'string': return isString(keyname, objectKey)
    case 'numeric': return isNumeric(keyname, objectKey)
    case 'int': return isInt(keyname, objectKey)
    case 'decimal': return isDecimal(keyname, objectKey)
    case 'alpha': return isAlpha(keyname, objectKey)
    case 'alphanumeric': return isAlphaNumeric(keyname, objectKey)
    case 'date': return isDate(keyname, objectKey)
    default:
      throw Error(`metadata for ${keyname} is invalid unsupported type given ${type}`)
  }
}

function isString (name, value) {
  if (typeof value === 'string') {
    return true
  } else {
    return toError(name, `invalid string provided for ${name}`)
  }
}

function isNumeric (name, value) {
  if (typeof value === 'number') {
    return true
  } else {
    return toError(name, `invalid number provided for ${name}`)
  }
}

function isInt (name, value) {
  const regex = /^[\d]*$/
  if (regex.test(value) && Number.isInteger(value)) {
    return true
  } else {
    return toError(name, `invalid number provided for ${name}`)
  }
}

function isDecimal (name, value) {
  if (!Number.isInteger(value) && typeof value === 'number') {
    return true
  } else {
    return toError(name, `invalid number provided for ${name}`)
  }
}

function isAlpha (name, value) {
  const regex = /^[a-zA-Z]*$/
  const error = toError(name, `invalid alphabets provided for ${name}`)

  if (typeof value === 'string') {
    return (regex.test(value)) ? true : error
  } else {
    return error
  }
}

function isAlphaNumeric (name, value) {
  const regex = /^[a-zA-Z0-9\s]+$/
  const error = toError(name, `invalid alphanumeric provided for ${name}`)

  if (typeof value === 'string') {
    return (regex.test(value)) ? true : error
  } else {
    return error
  }
}

function isDate (name, value) {
  const milliseconds = Date.parse(value)
  const error = toError(name, `invalid date provided for ${name}`)

  if (!Number.isNaN(milliseconds)) {
    return true
  } else {
    return error
  }
}

function toError (keyname, message) {
  return {
    key: keyname,
    message: message
  }
}
