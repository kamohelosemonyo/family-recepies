import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import routerMixin from './router/routermixin'
import controller from './controllers'
import validationMixin from './util/validatonmixin'
import './assets/js/bootstrap.bundle.min'
import './assets/css/bootstrap.min.css'
import './assets/css/component.css'
import './assets/css/layout.css'

const app = createApp(App)
app.use(router)
app.mixin(routerMixin)
app.mixin(controller)
app.mixin(validationMixin)
app.mount('#app')
